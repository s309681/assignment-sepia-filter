#include "file_work.h"

enum open_status file_open(FILE **file, const char *pathname,
                           const char *mode) {
    *file = fopen(pathname, mode);
    if(*file == NULL){
        return OPEN_ERROR;
    }else{
        return OPEN_OK;
    }
}

enum close_status file_close(FILE **file) {
  if (fclose(*file) == EOF){
    return CLOSE_ERROR;
  }else{
    free(file);
    return CLOSE_OK;
  }
}

