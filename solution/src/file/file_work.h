#ifndef FILE_WORK_H
#define FILE_WORK_H

#include <stdio.h>
#include <stdlib.h>

enum open_status { 
	OPEN_OK = 0, 
	OPEN_ERROR 
};
enum close_status { 
	CLOSE_OK = 0, 
	CLOSE_ERROR 
};

enum open_status file_open(FILE **file_link, const char *pathname, const char *mode);

enum close_status file_close(FILE **file_link);

#endif

