global sepia_asm

section .data

    c1_1: dd 0.131, 0.168, 0.189, 0.131
    c1_2: dd 0.543, 0.686, 0.769, 0.543
    c1_3: dd 0.272, 0.349, 0.393, 0.272


    c2_1: dd 0.168, 0.189, 0.131, 0.168
    c2_2: dd 0.686, 0.769, 0.543, 0.686
    c2_3: dd 0.349, 0.393, 0.272, 0.349

    c3_1: dd 0.189, 0.131, 0.168, 0.189
    c3_2: dd 0.769, 0.543, 0.686, 0.769
    c3_3: dd 0.393, 0.272, 0.349, 0.393

section .text

calculate:
    movdqa xmm3, [rsi]
    movdqa xmm4, [rsi+16]
    movdqa xmm5, [rsi+32]
    movdqu xmm0, [rdi]
    movdqu xmm1, [rdi+16]
    movdqu xmm2, [rdi+32]
    mulps  xmm0, xmm3
    mulps  xmm1, xmm4
    mulps  xmm2, xmm5
    addps  xmm0, xmm1
    addps  xmm0, xmm2
    movdqu [rdi], xmm0
    ret


sepia_asm:
    cmp rsi, 0
    jne .second
    mov rsi, c1_1
    call calculate
    ret
    .second:
    cmp rsi, 1
    jne .third
    mov rsi, c2_1
    call calculate
    ret
    .third:
    mov rsi, c3_1
    jmp calculate