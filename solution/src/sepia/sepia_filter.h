//
// Created by Ivan on 06.01.2022.
//

#ifndef ASSIGNMENT_SEPIA_FILTER_SEPIA_FILTER_H
#define ASSIGNMENT_SEPIA_FILTER_SEPIA_FILTER_H

#include "../image/image.h"
#include <inttypes.h>
#include <time.h>

struct image *sepia_filter_c(struct image* old_img);
struct image *sepia_filter_asm(struct image* old_img);

#endif //ASSIGNMENT_SEPIA_FILTER_SEPIA_FILTER_H
