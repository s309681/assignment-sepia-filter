#include "file/file_work.h"
#include "bmp/bmp.h"
#include <stdlib.h>
#include "sepia/sepia_filter.h"

int main(int argc, char **argv) {

    if (argc < 4) {
        fprintf(stderr, "Needs parameters: <source-image> <transformed-image> <0 - c, 1 - asm>");
        return -1;
    }

    FILE **input_bmp = malloc(sizeof(FILE * ));
    if (file_open(input_bmp, argv[1], "r") != 0) {
        fprintf(stderr, "Can't open file for read!");
        return -1;
    }

    FILE **output_bmp = malloc(sizeof(FILE * ));
    if (file_open(output_bmp, argv[2], "w") != 0) {
        fprintf(stderr, "Can't open file for write!");
        file_close(output_bmp);
        return -1;
    }

    struct image *original_image = image_create(0, 0);
    if (from_bmp(*input_bmp, original_image) != 0) {
        fprintf(stderr, "Can't create image from bmp!");
        file_close(output_bmp);
        return -1;
    }
    file_close(input_bmp);

    clock_t time = clock();
    struct image *rotated_image;
    if(*argv[3] == '0') {
        rotated_image = sepia_filter_c(original_image);
        time = clock() - time;
        printf("%ld - c-time\n", time);
    }
    if(*argv[3] == '1') {
        rotated_image = sepia_filter_asm(original_image);
        time = clock() - time;
        printf("%ld - asm-time\n", time);
    }

    if (to_bmp(*output_bmp, rotated_image) != 0) {
        fprintf(stderr, "Can't create bmp from image!");
        file_close(output_bmp);
        return -1;
    }
    file_close(output_bmp);

    image_destroy(original_image);
    image_destroy(rotated_image);

    return 0;
}

