#include "image.h"

struct image *image_create(uint64_t width, uint64_t height) {
    struct image *image = malloc(sizeof(struct image));
    image->width = width;
    image->height = height;
    return image;
}

struct image *image_copy(struct image *origin_image) {
    struct image* image = image_create(origin_image->width, origin_image->height);
    struct pixel *pixels = malloc(sizeof(struct pixel) * image->width * image->height);
    for (size_t i = 0; i < image->height * image->width; i++) {
        pixels[i] = origin_image->pixels[i];
    }
    image->pixels = pixels;
    return image;
}

void image_destroy(struct image *image) {
    free(image->pixels);
    free(image);
}

size_t get_height(struct image *image) {
    return image->height;
}

size_t get_width(struct image *image) {
    return image->width;
}

