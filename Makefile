CFLAGS=--std=c17 -Wall -pedantic -Isrc/ -ggdb -Wextra -Werror -DDEBUG
BUILDDIR=build
SRCDIR=solution/src
CC=gcc
ASM=nasm
ASMFLAGS=-felf64 -g
PHOTOS_DIR=photos

all: $(BUILDDIR)/main.o $(BUILDDIR)/bmp.o $(BUILDDIR)/image.o $(BUILDDIR)/sepia_filter.o $(BUILDDIR)/file_work.o $(BUILDDIR)/sepia_asm.o
	$(CC) -g -o $(BUILDDIR)/main $^

build:
	mkdir -p $(BUILDDIR)

$(BUILDDIR)/sepia_asm.o: $(SRCDIR)/sepia/sepia_asm.asm build
	$(ASM) $(ASMFLAGS) $< -o $@

$(BUILDDIR)/main.o: $(SRCDIR)/main.c build
	$(CC) -c $(CFLAGS) $< -o $@

$(BUILDDIR)/bmp.o: $(SRCDIR)/bmp/bmp.c build
	$(CC) -c $(CFLAGS) $< -o $@

$(BUILDDIR)/image.o: $(SRCDIR)/image/image.c build
	$(CC) -c $(CFLAGS) $< -o $@

$(BUILDDIR)/file_work.o: $(SRCDIR)/file/file_work.c build
	$(CC) -c $(CFLAGS) $< -o $@

$(BUILDDIR)/sepia_filter.o: $(SRCDIR)/sepia/sepia_filter.c build $(BUILDDIR)/sepia_asm.o
	$(CC) -c $(CFLAGS) $< -o $@

clean:
	rm -rf $(BUILDDIR)

run:
	./$(BUILDDIR)/main $(PHOTOS_DIR)/1.bmp $(PHOTOS_DIR)/2.bmp 1

debug:
	gdb ./$(BUILDDIR)/main